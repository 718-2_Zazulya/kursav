﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public static class Database
    {
        private static string dbName = $"{ Environment.CurrentDirectory }\\App_Data\\Database1.mdf";

        public static DataTable SendQuery(string query)
        {
            SqlConnection sqlConnection = new SqlConnection($"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={dbName};Integrated Security=True");
            sqlConnection.Open();
            SqlCommand command = new SqlCommand(query, sqlConnection);

            SqlDataAdapter chitat1 = new SqlDataAdapter(command);

            DataTable resultat = new DataTable();

            chitat1.Fill(resultat);

            sqlConnection.Close();

            return resultat;
        }
    }
}
