﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class EventMock : IEvent
    {
        public IEnumerable<Event> Events
        {
            get
            {
                return GetEvents();
            }
        }

        public EventMock()
        {

        }

        public void AddEvent(EventType typeOfEvent, double time, string text)
        {
            //string i_love_my_predovatel = $"INSERT [Event] VALUES ('{time}', '{(int)typeOfEvent}', '{text}')";
            Database.SendQuery($"INSERT [Event] VALUES ('{time}', '{(int)typeOfEvent}', '{text}')");
            
        }

        private IEnumerable<Event> GetEvents()
        {

            DataTable resultat = Database.SendQuery("SELECT * FROM [Event]");

            List<Event> events = new List<Event>();

            for (int i = 0; i < resultat.Rows.Count; i++)
            {
                EventType type = (EventType)int.Parse(resultat.Rows[i].ItemArray[1].ToString());
                Event currentEvent = new Event(type, int.Parse(resultat.Rows[i].ItemArray[2].ToString()), resultat.Rows[i].ItemArray[3].ToString());
                events.Add(currentEvent);
            }

            return events;
        }
    }
}
