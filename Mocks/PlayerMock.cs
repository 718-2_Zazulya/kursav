﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class PlayerMock: IPlayer
    {
        public IEnumerable<Player> Players
        {
            get
            {
                return GetPlayers();
            }
        }

        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Roles Role { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        private IEnumerable<Player> GetPlayers()
        {
            SqlConnection sqlConnection = new SqlConnection($"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={Environment.CurrentDirectory}\\App_Data\\Database1.mdf;Integrated Security=True");

            sqlConnection.Open();

            SqlCommand command = new SqlCommand("SELECT [Name] FROM [Plaers]", sqlConnection);

            SqlDataAdapter chitat = new SqlDataAdapter(command);

            DataTable resultat = new DataTable();

            chitat.Fill(resultat);

            List<Player> players = new List<Player>();

            for (int i = 0; i < resultat.Rows.Count; i++)
            {
                Player player = new Player(resultat.Rows[i].ItemArray[0].ToString(), Roles.PEOPLE);
                players.Add(player);
            }

            sqlConnection.Close();

            return players;
        }
    }
}