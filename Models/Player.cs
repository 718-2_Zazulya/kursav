﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Enum;

namespace WebApplication1.Models
{
    public class Player
    {
        private string name;
        private Roles role;

        public Player(string _name, Roles _role)
        {
            name = _name;
            role = _role;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }


        public Roles Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
            }
        }
    }
}