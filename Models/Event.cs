﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Enum;

namespace WebApplication1.Models
{
    public class Event
    {
        private EventType type;
        private double time;
        private string about;

        public Event(EventType _type, double _time, string _about)
        {
            type = _type;
            time = _time;
            about = _about;
        }

        public EventType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public double Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }
        public string Text
        {
            get
            {
                return about;
            }
            set
            {
                about = value;
            }
        }
    }
}