﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Mocks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EventController : Controller
    {
        private readonly IEvent events;
        private Dictionary<Player, bool> isPlayersReady;
        private bool gameIsEnded = false;
        public EventController(IEvent _events)
        {
            events = _events;
            isPlayersReady = new Dictionary<Player, bool>();
        }

        public async void ItsGameBaby()
        {
            int eventCount = 1;
            while (!gameIsEnded)
            {
                int time = StartEvent(eventCount);
                switch (eventCount)
                {
                    case 1:
                        eventCount = 2;
                        break;

                    case 2: //голосование день
                        eventCount = 3;
                        break;
                    case 3:
                        KillSomebody();
                        eventCount = 4;
                        break;
                    case 4:
                        //ночное голосование 
                        KillNight();
                        eventCount = 5;
                        break;
                    case 5:
                        //TheEnd();
                        eventCount = 1;
                        break;

                }
                Task zadershka = Task.Delay(time * 1000);

                await zadershka;
            }
        }
        


        private void KillSomebody()
        {
            DataTable players = Database.SendQuery($"SELECT * FROM [Plaers]");
            int max = 0;
            string name = "putin";
            for (int i = 0; i < players.Rows.Count; i++)
            {
                int votes = int.Parse(players.Rows[i].ItemArray[5].ToString());
                if (votes > max)
                {
                    max = votes;
                    name = players.Rows[i].ItemArray[1].ToString();
                }
            }

            if (name != "putin")
            {
                Database.SendQuery($"UPDATE Plaers set isDead = 'true' WHERE name = '{name}'");
                events.AddEvent(EventType.Текст, 0, $"Player {name} was punished!");
            }
            else
            {
                events.AddEvent(EventType.Текст, 0, $"Nobody was punished.");
            }

            if (CheckDead(Roles.PEOPLE))
            {
                events.AddEvent(EventType.Окончание, 0, $"Are you winning, mafia?");
                gameIsEnded = true;
            }
            else if (CheckDead(Roles.Looser))
            {
                events.AddEvent(EventType.Окончание, 0, $"And no one was there");
                gameIsEnded = true;
            }
            else if (CheckDead(Roles.MAFIA))
            {
                events.AddEvent(EventType.Окончание, 0, $"Putin molodec");
                gameIsEnded = true;
            }

            /*
            DataTable currentEvents = Database.SendQuery($"SELECT Type FROM [Event]");
            int lastRow = currentEvents.Rows.Count - 1;
            int eventType = int.Parse(currentEvents.Rows[lastRow].ItemArray[0].ToString());
            if (eventType == (int)EventType.Окончание)
            {
                gameIsEnded = true;
            }*/
        }

        private bool CheckDead(Roles checkRole = Roles.Looser)
        {
            DataTable currentEvents = Database.SendQuery($"SELECT * FROM [Plaers]");
            for (int i = 0; i < currentEvents.Rows.Count; i++)
            {
                object[] human = currentEvents.Rows[i].ItemArray;
                int role = int.Parse(human[2].ToString());
                bool isDead = bool.Parse(human[6].ToString());
                //проверка для всех ролей
                if (checkRole == Roles.Looser)
                {
                    if (!isDead)
                    {
                        return false;
                    }
                    else if (i == currentEvents.Rows.Count - 1)
                    {
                        return true;
                    }
                }
                else if (checkRole == Roles.MAFIA)
                {
                    if (((role == (int)Roles.MAFIA) || (role == (int)Roles.DON_MAFIA)) && (!isDead))
                    {
                        return false;
                    }
                    else if (i == currentEvents.Rows.Count - 1)
                    {
                        return true;
                    }
                }
                else if (checkRole == Roles.PEOPLE)
                {
                    if ((role == (int)Roles.PEOPLE) || (role == (int)Roles.SHERIF) && !isDead)
                    {
                        return false;
                    }
                    else if (i == currentEvents.Rows.Count - 1)
                    {
                        return true;
                    }
                }
            }
            throw new Exception("Ты лох");
        }


        public void KillNight()
        {
            DataTable players = Database.SendQuery($"SELECT * FROM [Plaers]");
            int max = 0;
            string name = "Putin";
            for (int i = 0; i < players.Rows.Count; i++)
            {
                int votes = int.Parse(players.Rows[i].ItemArray[5].ToString());
                if (votes > max)
                {
                    max = votes;
                    name = players.Rows[i].ItemArray[1].ToString();
                }
            }
            if (name != "Putin")
            {
                Database.SendQuery($"UPDATE Plaers set isDead = 'true' WHERE name = '{name}'");
                events.AddEvent(EventType.Текст, 0, $"Player {name} was punished!");
            }
            else
            {
                events.AddEvent(EventType.Текст, 0, $"Nobody was punished.");
            }

            if (CheckDead(Roles.PEOPLE))
            {
                events.AddEvent(EventType.Окончание, 0, $"Are you winning, mafia?");
                gameIsEnded = true;
            }
            else if (CheckDead(Roles.Looser))
            {
                events.AddEvent(EventType.Окончание, 0, $"And no one was there");
                gameIsEnded = true;
            }
            else if (CheckDead(Roles.MAFIA))
            {
                events.AddEvent(EventType.Окончание, 0, $"Putin molodec");
                gameIsEnded = true;
            }
        }
        public void TheEnd()
        {
            DataTable players = Database.SendQuery($"SELECT * FROM [Plaers]");
            int max = 0;
            string name = "pososesh";
            for (int i = 0; i < players.Rows.Count; i++)
            {
                int votes = int.Parse(players.Rows[i].ItemArray[5].ToString());
                if (votes > max)
                {
                    max = votes;
                    name = players.Rows[i].ItemArray[1].ToString();
                }
            }
            if (name != "pososesh")
            {
                Database.SendQuery($"UPDATE Plaers set isDead = 'true'");
                events.AddEvent(EventType.Окончание, 0, $"The End");
            }
            else
            {
                events.AddEvent(EventType.Окончание, 0, $"Game continues");
            }
        }

        //public void MafiaResult()
        //{
        //    DataTable players = Database.SendQuery($"SELECT * FROM [Plaers]");
        //    int max = 0;
        //    string name = "pososesh";
        //    for (int i = 0; i<)
        //        {

        //    }
        //}
           


        public ViewResult EventsList()
        {
            var suBevents = Database.SendQuery($"SELECT * FROM [Event]").Rows;
            if (suBevents.Count == 1)
            {
                ItsGameBaby();
            }
            return View(events.Events);
        }

        // GET: EventController
        public int StartEvent(int count)
        {
            int time = 10;
            EventType type = EventType.День;
            string text = "Morning sun is waking up";
            switch (count)
            {
                case 2:
                    type = EventType.Голосование_день;
                    text = "Kill friend or die";
                    break;
                case 3:
                    type = EventType.Ночь;
                    text = "Good night sweet prince";
                    break;
                case 4:
                    type = EventType.Голосование_ночь;
                    text = "Kill friend or die";
                    break;
                case 5:
                    type = EventType.Окончание;
                    break;
            }
            Database.SendQuery($"INSERT [Event] VALUES ('{time}', '{(int)type}', '{text}')");
            return time;
        }

    }
}
