﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class PlayerController: Controller
    {
        private readonly IPlayer players;

        public PlayerController(IPlayer _players)
        {
            players = _players;
        }
        
        public ViewResult List()
        {
            IEnumerable<Player> list = players.Players;
            return View(list);
        }

        public IActionResult GetVote(string playerName)
        {
            string name = Request.Cookies["name"];
            DataTable me = Database.SendQuery($"SELECT * FROM [Plaers] Where Name = '{name}'");
            DataTable player = Database.SendQuery($"SELECT * FROM [Plaers] Where Name = '{playerName}'");
            DataTable currentEvent = Database.SendQuery($"SELECT Type FROM [Event]");
            bool isDay = int.Parse(currentEvent.Rows[currentEvent.Rows.Count - 1].ItemArray[0].ToString()) == (int)EventType.Голосование_день;
            bool isNight = int.Parse(currentEvent.Rows[currentEvent.Rows.Count - 1].ItemArray[0].ToString()) == (int)EventType.Голосование_ночь;
            bool isMafia = int.Parse(me.Rows[0].ItemArray[2].ToString()) == (int)Roles.MAFIA;
            bool isDonMafia = int.Parse(me.Rows[0].ItemArray[2].ToString()) == (int)Roles.DON_MAFIA;
            if (isDay || (isNight && (isMafia || isDonMafia)))
            {
                if (Request.Cookies["Voted"] == "False" && me.Rows[0].ItemArray[6].ToString() == "false     " && player.Rows[0].ItemArray[6].ToString() == "false     ")
                {
                    Database.SendQuery($"UPDATE Plaers set Votes = '{int.Parse(player.Rows[0].ItemArray[5].ToString()) + 1}' WHERE Name = '{playerName}'");
                    CookieOptions options = new CookieOptions();
                    Response.Cookies.Append("Voted", "True", options);
                }
            }
            return Redirect("list");
        }

        public ViewResult Game()
        {
            DataTable events = Database.SendQuery($"SELECT * FROM [Event]");
            if (events.Rows.Count == 0)
            {
                Database.SendQuery($"DELETE FROM Plaers");
            }
            var subPlayers = Database.SendQuery($"SELECT * FROM [Plaers]").Rows;
            string name = Request.Cookies["name"];
            bool isPlayerBe = false;
            for (int i = 0; i < subPlayers.Count; i++)
            {
                if (subPlayers[i].ItemArray[1].ToString() == name)
                {
                    isPlayerBe = true;
                    break;
                }
                else
                {
                    isPlayerBe = false;
                }
            }

            if (!isPlayerBe)
            {
                Random rnd = new Random();
                CookieOptions options = new CookieOptions();
                Response.Cookies.Append("Voted", "False", options);
                Database.SendQuery($"INSERT [Plaers] VALUES ('{name}', '{(int)Roles.PEOPLE}', 'false', '{rnd.Next(1000, 9999)}', '0', 'false')");
            }
                

            return View(players.Players);
        }

        public IActionResult StartGame()
        {
            string name = Request.Cookies["name"];
            Database.SendQuery($"UPDATE Plaers set isReady = 'true' WHERE Name = '{name}'");
            bool isAllReady = true;
            var players = Database.SendQuery("SELECT * FROM [Plaers]").Rows; 
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].ItemArray[3].ToString() == "false")
                {
                    isAllReady = false;
                    break;
                }
                else
                {
                    isAllReady = true;
                }
            }

            if (isAllReady)
            {
                Database.SendQuery($"INSERT [Event] VALUES ('5', '{(int)EventType.Старт}', 'Game is started')");
                DataTable currentPlayers = Database.SendQuery($"Select * from Plaers order by RandomId");
                for (int i = 0; i < currentPlayers.Rows.Count; i++)
                {
                    GenerateRoles(currentPlayers.Rows[i].ItemArray[1].ToString());
                }
            }

            return Redirect("game");
        }

        public ActionResult GetPlayer()
        {
            return PartialView(players.Players);
        }

        public void GenerateRoles(string name)
        {
            bool isGenerated = false;
            Random random = new Random();
            while (!isGenerated)
            {
                int randomNumber = random.Next(0, 10);
                (List<bool> mafiaIsBe, bool dommafiaIsBe, bool sherifIsBe) = CheckAllRoles();
                if (mafiaIsBe.Count < 2 && randomNumber < 3)
                {
                    SetRole(Roles.MAFIA, name);
                    isGenerated = true;
                }
                else if (!dommafiaIsBe && randomNumber >=3 && randomNumber < 6)
                {
                    SetRole(Roles.DON_MAFIA, name);
                    isGenerated = true;
                }
                else if (!sherifIsBe && randomNumber >= 6)
                {
                    SetRole(Roles.SHERIF, name);
                    isGenerated = true;
                }
                else if (dommafiaIsBe && sherifIsBe && mafiaIsBe.Count >= 2)
                {
                    isGenerated = true;
                }
            }
        }

        private void SetRole(Roles role, string name)
        {
            Database.SendQuery($"UPDATE Plaers set Rol ='{(int)role}' WHERE Name = '{name}'");
        }

        private (List<bool> mafiaIsBe, bool dommafiaIsBe, bool sherifIsBe) CheckAllRoles()
        {
            DataTable roles =  Database.SendQuery($"SELECT Rol From Plaers");
            List<bool> mafiaIsBe = new List<bool>();
            bool dommafiaIsBe = false;
            bool sherifIsBe = false;
            for (int i = 0; i < roles.Rows.Count; i++)
            {
                if (roles.Rows[i].ItemArray[0].ToString() == "0")
                {
                    mafiaIsBe.Add(true);
                }
                else if (roles.Rows[i].ItemArray[0].ToString() == "1")
                {
                    dommafiaIsBe = true;
                }
                else if (roles.Rows[i].ItemArray[0].ToString() == "2")
                {
                    sherifIsBe = true;
                }
            }
            return (mafiaIsBe, dommafiaIsBe, sherifIsBe);
        }
    }
}