﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Enum
{
    public enum EventType
    {
        Старт,
        День,
        Голосование_день,
        Ночь,
        Голосование_ночь,
        Окончание,
        Присоединение,
        Создание,
        Текст,
    }
}